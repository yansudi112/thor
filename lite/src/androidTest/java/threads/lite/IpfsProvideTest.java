package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsProvideTest {
    private static final String TAG = IpfsProvideTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_resolve_provide() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session session = ipfs.createSession()) {
            LogUtils.debug(TAG, ipfs.self().toBase58());
            byte[] data = TestEnv.getRandomBytes(100);
            Cid cid = ipfs.storeData(session, data);
            assertNotNull(cid);

            long start = System.currentTimeMillis();

            Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
            ipfs.provide(session, cid, providers::add,
                    new TimeoutCancellable(() -> false, 30));

            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            long time = System.currentTimeMillis();
            AtomicReference<Multiaddr> lastPeer = new AtomicReference<>(null);

            ipfs.findProviders(session, lastPeer::set, cid,
                    new TimeoutCancellable(() -> lastPeer.get() != null, 30));


            LogUtils.debug(TAG, "Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");
            assertNotNull(lastPeer.get());
        }
    }


    @Test
    public void test_host_provide() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session session = ipfs.createSession()) {
            PeerId self = ipfs.self();

            Cid cid = Cid.decode(self.toBase36());
            assertNotNull(cid);

            boolean result = Arrays.equals(cid.getMultihash(), self.getBytes());
            assertTrue(result);

            long start = System.currentTimeMillis();

            Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
            ipfs.provide(session, cid, providers::add,
                    new TimeoutCancellable(() -> false, 30));

            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            long time = System.currentTimeMillis();
            AtomicBoolean finished = new AtomicBoolean(false);

            ipfs.findProviders(session, (multiaddr) -> finished.set(true), cid,
                    new TimeoutCancellable(finished::get, 30));

            LogUtils.debug(TAG, "Time Providers : " +
                    (System.currentTimeMillis() - time) + " [ms]");
            assertTrue(finished.get());
        }
    }

    //@Test
    public void test_host_publish_find() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            PeerId self = ipfs.self();

            LogUtils.debug(TAG, self.toBase58());


            long start = System.currentTimeMillis();

            Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();

            ipfs.publishName(session, 0, "hallo".getBytes(StandardCharsets.UTF_8),
                    providers::add, new TimeoutCancellable(() -> false, 30));

            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            long time = System.currentTimeMillis();
            IpnsEntity res = ipfs.resolveName(session, self, 0,
                    new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.toString());

            LogUtils.debug(TAG, "Time Find Peer : " +
                    (System.currentTimeMillis() - time) + " [ms]");
        }
    }
}
