package threads.lite;


import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsFindPeerTest {
    private static final String TAG = IpfsFindPeerTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void find_peer_corbett() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //CorbettReport ipns://k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm

        try (Session session = ipfs.createSession()) {
            String key = "k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm";
            IpnsEntity res = ipfs.resolveName(session, PeerId.fromString(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            assertNotNull(res.getData());
            LogUtils.debug(TAG, res.toString());

        }
    }

    @Test
    public void find_peer_freedom() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //FreedomsPhoenix.com ipns://k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1

        try (Session session = ipfs.createSession()) {
            String key = "k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1";
            IpnsEntity res = ipfs.resolveName(session, PeerId.fromString(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            assertNotNull(res.getData());
            LogUtils.debug(TAG, res.toString());

        }
    }

    @Test
    public void find_peer_pirates() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //PiratesWithoutBorders.com ipns://k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t

        try (Session session = ipfs.createSession()) {
            String key = "k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t";
            IpnsEntity res = ipfs.resolveName(session, PeerId.fromString(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            assertNotNull(res.getData());
            LogUtils.debug(TAG, res.toString());

        }
    }
}
