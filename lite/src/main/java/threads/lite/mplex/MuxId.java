package threads.lite.mplex;

import androidx.annotation.NonNull;

import java.util.Objects;

public class MuxId {
    private final int streamId;
    private final boolean initiator;

    public MuxId(int streamId, boolean initiator) {
        this.streamId = streamId;
        this.initiator = initiator;
    }

    @NonNull
    @Override
    public String toString() {
        return "MuxId{" +
                "streamId=" + streamId +
                ", initiator=" + initiator +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MuxId muxId = (MuxId) o;
        return streamId == muxId.streamId && initiator == muxId.initiator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(streamId, initiator);
    }

    public int getStreamId() {
        return streamId;
    }

    public boolean isInitiator() {
        return initiator;
    }
}
