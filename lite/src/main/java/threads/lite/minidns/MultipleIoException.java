/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class MultipleIoException extends IOException {


    private MultipleIoException(List<? extends IOException> ioExceptions) {
        super(getMessage(ioExceptions));
        assert !ioExceptions.isEmpty();
    }

    private static String getMessage(Collection<? extends Exception> exceptions) {
        StringBuilder sb = new StringBuilder();
        Iterator<? extends Exception> it = exceptions.iterator();
        while (it.hasNext()) {
            sb.append(it.next().getMessage());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public static void throwIfRequired(List<? extends IOException> ioExceptions) throws IOException {
        if (ioExceptions == null || ioExceptions.isEmpty()) {
            return;
        }
        if (ioExceptions.size() == 1) {
            throw ioExceptions.get(0);
        }
        throw new MultipleIoException(ioExceptions);
    }

}
