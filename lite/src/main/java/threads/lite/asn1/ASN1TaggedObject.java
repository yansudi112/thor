package threads.lite.asn1;

import androidx.annotation.NonNull;

import java.io.IOException;

import threads.lite.asn1.util.Arrays;

/**
 * ASN.1 TaggedObject - in ASN.1 notation this is any object preceded by
 * a [n] where n is some number - these are assumed to follow the construction
 * rules (as with sequences).
 */
public abstract class ASN1TaggedObject extends ASN1Primitive implements ASN1Encodable, InMemoryRepresentable {
    private static final int DECLARED_EXPLICIT = 1;
    private static final int DECLARED_IMPLICIT = 2;
    private static final int PARSED_EXPLICIT = 3;
    private static final int PARSED_IMPLICIT = 4;
    final int explicitness;
    final int tagClass;
    final int tagNo;
    final ASN1Encodable obj;

    /**
     * Create a tagged object with the style given by the value of explicit.
     * <p>
     * If the object implements ASN1Choice the tag style will always be changed
     * to explicit in accordance with the ASN.1 encoding rules.
     * </p>
     *
     * @param explicit true if the object is explicitly tagged.
     * @param tagNo    the tag number for this object.
     * @param obj      the tagged object.
     */


    protected ASN1TaggedObject(boolean explicit, int tagNo, ASN1Encodable obj) {
        this(explicit ? DECLARED_EXPLICIT : DECLARED_IMPLICIT, BERTags.CONTEXT_SPECIFIC, tagNo, obj);
    }

    ASN1TaggedObject(int explicitness, int tagClass, int tagNo, ASN1Encodable obj) {
        if (null == obj) {
            throw new NullPointerException("'obj' cannot be null");
        }
        if (tagClass == BERTags.UNIVERSAL) {
            throw new IllegalArgumentException("invalid tag class: " + tagClass);
        }

        this.explicitness = (obj instanceof ASN1Choice) ? DECLARED_EXPLICIT : explicitness;
        this.tagClass = tagClass;
        this.tagNo = tagNo;
        this.obj = obj;
    }


    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1TaggedObject)) {
            return false;
        }

        ASN1TaggedObject that = (ASN1TaggedObject) other;

        if (this.tagNo != that.tagNo ||
                this.tagClass != that.tagClass) {
            return false;
        }

        if (this.explicitness != that.explicitness) {
            if (this.isExplicit() != that.isExplicit()) {
                return false;
            }
        }

        ASN1Primitive p1 = this.obj.toASN1Primitive();
        ASN1Primitive p2 = that.obj.toASN1Primitive();

        if (p1 == p2) {
            return true;
        }

        if (!this.isExplicit()) {
            try {
                byte[] d1 = this.getEncoded();
                byte[] d2 = that.getEncoded();

                return Arrays.areEqual(d1, d2);
            } catch (IOException e) {
                return false;
            }
        }

        return p1.asn1Equals(p2);
    }

    public int hashCode() {
        return (tagClass * 7919) ^ tagNo ^ (isExplicit() ? 0x0F : 0xF0) ^ obj.toASN1Primitive().hashCode();
    }

    public int getTagClass() {
        return tagClass;
    }

    /**
     * Return the tag number associated with this object.
     *
     * @return the tag number.
     */
    public int getTagNo() {
        return tagNo;
    }

    /**
     * return whether or not the object may be explicitly tagged.
     * <p>
     * Note: if the object has been read from an input stream, the only
     * time you can be sure if isExplicit is returning the true state of
     * affairs is if it returns false. An implicitly tagged object may appear
     * to be explicitly tagged, so you need to understand the context under
     * which the reading was done as well, see getObject below.
     */
    public boolean isExplicit() {

        switch (explicitness) {
            case DECLARED_EXPLICIT:
            case PARSED_EXPLICIT:
                return true;
            default:
                return false;
        }
    }


    ASN1Primitive getBaseUniversal(boolean declaredExplicit, ASN1UniversalType universalType) {
        if (declaredExplicit) {
            if (!isExplicit()) {
                throw new IllegalStateException("object explicit - implicit expected.");
            }

            return universalType.checkedCast(obj.toASN1Primitive());
        }

        if (DECLARED_EXPLICIT == explicitness) {
            throw new IllegalStateException("object explicit - implicit expected.");
        }

        ASN1Primitive primitive = obj.toASN1Primitive();
        switch (explicitness) {
            case PARSED_EXPLICIT:
                return universalType.fromImplicitConstructed(rebuildConstructed(primitive));
            case PARSED_IMPLICIT: {
                if (primitive instanceof ASN1Sequence) {
                    return universalType.fromImplicitConstructed((ASN1Sequence) primitive);
                }
                return universalType.fromImplicitPrimitive((DEROctetString) primitive);
            }
            default:
                return universalType.checkedCast(primitive);
        }
    }


    public final ASN1Primitive getLoadedObject() {
        return this;
    }

    abstract ASN1Sequence rebuildConstructed(ASN1Primitive primitive);

    ASN1Primitive toDERObject() {
        return new DERTaggedObject(explicitness, tagClass, tagNo, obj);
    }

    ASN1Primitive toDLObject() {
        return new DLTaggedObject(explicitness, tagClass, tagNo, obj);
    }

    @NonNull
    public String toString() {
        return ASN1Util.getTagText(tagClass, tagNo) + obj;
    }
}
