package threads.lite.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import threads.lite.LogUtils;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;
import threads.lite.core.Cancellable;

public class Query {

    private static final String TAG = Query.class.getSimpleName();

    private static List<QueryPeer> transform(@NonNull ID key, @NonNull List<Peer> peers) {
        List<QueryPeer> result = new ArrayList<>();
        peers.forEach(peer -> result.add(QueryPeer.create(peer, key)));
        return result;
    }

    public static void runQuery(@NonNull KadDht dht, @NonNull Cancellable cancellable,
                                @NonNull ID key, @NonNull List<QueryPeer> peers,
                                @NonNull KadDht.QueryFunc queryFn) throws InterruptedException {

        int threads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        try {
            QueryPeerSet queryPeers = new QueryPeerSet();
            enhanceSet(dht, queryPeers, peers);

            iteration(dht, executorService, queryPeers, cancellable, key, queryFn, threads);

            boolean termination = executorService.awaitTermination(
                    cancellable.timeout(), TimeUnit.SECONDS);
            LogUtils.info(TAG, "Termination " + termination);
        } finally {
            executorService.shutdown();
            executorService.shutdownNow();
        }
    }

    private static boolean enhanceSet(KadDht dht, QueryPeerSet queryPeers, List<QueryPeer> peers) {
        // the peers in query update are added to the queryPeers
        boolean enhanceSet = false;
        for (QueryPeer peer : peers) {
            if (Objects.equals(peer.getPeer().getPeerId(), dht.self)) { // don't add self.
                continue;
            }
            boolean result = queryPeers.tryAdd(peer);  // set initial state to PeerHeard
            if (result) {
                enhanceSet = true;
            }
        }
        return enhanceSet;
    }


    public static void iteration(KadDht dht, ExecutorService executorService,
                                 QueryPeerSet queryPeers, Cancellable cancellable, ID key,
                                 KadDht.QueryFunc queryFn, int threads) throws InterruptedException {

        if (cancellable.isCancelled()) {
            executorService.shutdown();
            throw new InterruptedException("operation canceled");
        }

        List<QueryPeer> nextPeersToQuery = queryPeers.nextHeardPeers(threads);

        for (QueryPeer queryPeer : nextPeersToQuery) {
            queryPeer.setState(PeerState.PeerWaiting);
            try {
                if (!executorService.isShutdown()) {
                    executorService.execute(() -> {
                        try {
                            long start = System.currentTimeMillis();

                            List<Peer> newPeers = queryFn.query(cancellable, queryPeer.getPeer());
                            queryPeer.setState(PeerState.PeerQueried);

                            boolean enhancedSet = enhanceSet(dht, queryPeers,
                                    transform(key, newPeers));

                            // query successful, try to add to routing table
                            if (enhancedSet) {
                                // only when they the query peer is returning something
                                // it will be added
                                long latency = System.currentTimeMillis() - start;
                                long metric = latency / newPeers.size();
                                queryPeer.getPeer().setMetric(metric);
                                dht.addToRouting(queryPeer);
                            }

                            iteration(dht, executorService, queryPeers, cancellable,
                                    key, queryFn, threads);

                        } catch (InterruptedException ignore) {
                            executorService.shutdown();
                        } catch (ConnectException connectException) {
                            dht.removeFromRouting(queryPeer);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            dht.removeFromRouting(queryPeer);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } finally {
                            if (queryPeers.isUnreachable()) {
                                executorService.shutdown();
                            }
                        }
                    });
                }
            } catch (RejectedExecutionException ignore) {
                // standard exception
            }
        }

    }

}
