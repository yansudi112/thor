package threads.lite.cert;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

import threads.lite.asn1.x509.Certificate;

/**
 * Holding class for an X.509 Certificate structure.
 */
public class X509CertificateHolder implements Serializable {
    private static final long serialVersionUID = 20170722001L;

    private transient Certificate x509Certificate;

    /**
     * Create a X509CertificateHolder from the passed in ASN.1 structure.
     *
     * @param x509Certificate an ASN.1 Certificate structure.
     */
    public X509CertificateHolder(Certificate x509Certificate) {
        init(x509Certificate);
    }

    private void init(Certificate x509Certificate) {
        this.x509Certificate = x509Certificate;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof X509CertificateHolder)) {
            return false;
        }

        X509CertificateHolder other = (X509CertificateHolder) o;

        return this.x509Certificate.equals(other.x509Certificate);
    }

    public int hashCode() {
        return this.x509Certificate.hashCode();
    }

    /**
     * Return the ASN.1 encoding of this holder's certificate.
     *
     * @return a DER encoded byte array.
     * @throws IOException if an encoding cannot be generated.
     */
    public byte[] getEncoded() throws IOException {
        return x509Certificate.getEncoded();
    }

    private void readObject(ObjectInputStream in) throws Exception {
        in.defaultReadObject();

        init(Objects.requireNonNull(Certificate.getInstance(in.readObject())));
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();

        out.writeObject(this.getEncoded());
    }
}
