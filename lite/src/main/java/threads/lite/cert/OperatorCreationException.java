package threads.lite.cert;

public class OperatorCreationException
        extends OperatorException {
    public OperatorCreationException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
