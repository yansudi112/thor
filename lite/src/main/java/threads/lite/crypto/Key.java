package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.crypto.tink.subtle.Ed25519Sign;
import com.google.crypto.tink.subtle.Ed25519Verify;
import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

import crypto.pb.Crypto;
import threads.lite.IPFS;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.Keys;
import threads.lite.utils.DataHandler;

public interface Key {

    static byte[] createIpnsKey(@NonNull PeerId peerId) {
        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        byte[] selfKey = peerId.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(ipns.length + selfKey.length);
        byteBuffer.put(ipns);
        byteBuffer.put(selfKey);
        return byteBuffer.array();
    }

    @NonNull
    static PeerId decodeIpnsKey(byte[] ipnsKey) throws Exception {
        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        int index = DataHandler.indexOf(ipnsKey, ipns);
        if (index != 0) {
            throw new Exception("parsing issue");
        }
        byte[] pid = Arrays.copyOfRange(ipnsKey, ipns.length, ipnsKey.length);
        Multihash mh = Multihash.deserialize(pid);
        return PeerId.fromBase58(mh.toBase58());
    }

    static PubKey unmarshalPublicKey(byte[] data) throws Exception {

        Crypto.PublicKey pms = Crypto.PublicKey.parseFrom(data);

        byte[] pubKeyData = pms.getData().toByteArray();

        switch (pms.getType()) {
            case RSA:
                return Rsa.unmarshalRsaPublicKey(pubKeyData);
            case ECDSA:
                return Ecdsa.unmarshalEcdsaPublicKey(pubKeyData);
            case Secp256k1:
                return Secp256k1.unmarshalSecp256k1PublicKey(pubKeyData);
            case Ed25519:
                return Ed25519.unmarshalEd25519PublicKey(pubKeyData);
            default:
                throw new Exception("BadKeyTypeException");
        }
    }

    // Note: Only Ed25519 support
    static byte[] sign(byte[] privateKey, byte[] data) throws Exception {
        Objects.requireNonNull(privateKey);
        Ed25519Sign signer = new Ed25519Sign(privateKey);
        return signer.sign(data);
    }

    // Note: Only Ed25519 support
    static Crypto.PublicKey createCryptoKey(byte[] publicKey) {
        Objects.requireNonNull(publicKey);
        return Crypto.PublicKey.newBuilder().setType(crypto.pb.Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(publicKey))
                .build();
    }

    // Note: Only Ed25519 support
    static PeerId createPeerId(byte[] publicKey) throws Exception {
        Ed25519.Ed25519PublicKey pubKey = new Ed25519.Ed25519PublicKey(publicKey);
        Objects.requireNonNull(pubKey);
        return PeerId.fromPubKey(pubKey);
    }

    // Note: Only Ed25519 support
    static void verify(byte[] publicKey, byte[] data, byte[] signature) throws Exception {
        // get the publicKey from the other party.
        Ed25519Verify verifier = new Ed25519Verify(publicKey);
        verifier.verify(signature, data);
    }

    // Note: Only Ed25519 support
    static Keys generateKeys() throws Exception {
        Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
        return new Keys(keyPair.getPublicKey(), keyPair.getPrivateKey());
    }


    @NonNull
    byte[] raw();
}


