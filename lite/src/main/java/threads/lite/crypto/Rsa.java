package threads.lite.crypto;

import androidx.annotation.NonNull;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import crypto.pb.Crypto;


public class Rsa {

    public static PubKey unmarshalRsaPublicKey(byte[] keyBytes) throws Exception {

        PublicKey publicKey = KeyFactory.getInstance("RSA")
                .generatePublic(new X509EncodedKeySpec(keyBytes));
        return new RsaPublicKey(publicKey);

    }


    public static final class RsaPublicKey extends PubKey {
        private final PublicKey publicKey;

        public RsaPublicKey(PublicKey publicKey) {
            super(Crypto.KeyType.RSA);
            this.publicKey = publicKey;
        }


        @NonNull
        public byte[] raw() {
            return this.publicKey.getEncoded();
        }

        public void verify(byte[] data, byte[] signature) throws Exception {

            Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
            sha256withRSA.initVerify(this.publicKey);
            sha256withRSA.update(data);
            boolean result = sha256withRSA.verify(signature);
            if (!result) {
                throw new Exception("verify failed");
            }
        }

        public int hashCode() {
            return this.publicKey.hashCode();
        }
    }
}
