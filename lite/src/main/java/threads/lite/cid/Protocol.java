package threads.lite.cid;

import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public enum Protocol {

    IP4(4, 32, Type.IP4),
    TCP(6, 16, Type.TCP),
    IP6(41, 128, Type.IP6),
    DNS(53, -1, Type.DNS),
    DNS4(54, -1, Type.DNS4),
    DNS6(55, -1, Type.DNS6),
    DNSADDR(56, -1, Type.DNSADDR),
    UDP(273, 16, Type.UDP),
    P2P(421, -1, Type.P2P),
    P2PCIRCUIT(290, 0, Type.P2PCIRCUIT),
    QUIC(460, 0, Type.QUIC);

    public static final String IPV4_REGEX = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
    private static final Map<String, Protocol> byName = new HashMap<>();
    private static final SparseArray<Protocol> byCode = new SparseArray<>();

    static {
        for (Protocol t : Protocol.values()) {
            byName.put(t.getType(), t);
            byCode.put(t.code(), t);
        }
    }

    private final int code, size;
    private final String type;
    private final byte[] encoded;

    Protocol(int code, int size, String type) {
        this.code = code;
        this.size = size;
        this.type = type;
        this.encoded = encode(code);
    }

    public static Protocol get(String name) {
        if (byName.containsKey(name))
            return byName.get(name);
        throw new IllegalStateException("No protocol with name: " + name);
    }

    public static Protocol get(int code) {
        Protocol protocol = byCode.get(code);
        if (protocol == null) {
            throw new IllegalStateException("No protocol with code: " + code);
        }
        return protocol;
    }

    static byte[] encode(int code) {
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        return varint;
    }

    static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }

    public static List<String> createAddress(InetSocketAddress inetSocketAddress) {
        InetAddress inetAddress = inetSocketAddress.getAddress();
        boolean ipv6 = inetAddress instanceof Inet6Address;
        int port = inetSocketAddress.getPort();
        List<String> parts = new ArrayList<>();
        if (ipv6) {
            parts.add(Type.IP6);
        } else {
            parts.add(Type.IP4);
        }
        parts.add(inetAddress.getHostAddress());
        parts.add(Type.UDP);
        parts.add(String.valueOf(port));
        parts.add(Type.QUIC);
        return parts;

    }

    public static byte[] decode(InetSocketAddress inetSocketAddress, PeerId relayId) {
        List<String> parts = createAddress(inetSocketAddress);
        parts.add(Type.P2P);
        parts.add(relayId.toBase58());
        parts.add(Type.P2PCIRCUIT);
        return decode(parts);
    }

    public static List<String> reducePeerId(PeerId peerId, List<String> parts) {
        // not nice, but it removes the p2p part when peerId is the same
        int size = parts.size();
        if (size >= 2) {
            String peerBase58 = parts.get(size - 1);
            if (Objects.equals(peerBase58, peerId.toBase58())) {
                return parts.subList(0, size - 2);
            }
        }
        return parts;
    }

    public static PeerId getPeerId(String address) throws Exception {
        List<String> list = Protocol.getPeerIds(address);
        if (list.size() == 0) {
            throw new Exception("Invalid address");
        }
        return PeerId.fromString(list.get(list.size() - 1));
    }

    @NonNull
    public static List<String> getPeerIds(String address) {
        String[] tokens = address.split("/");
        List<String> result = new ArrayList<>();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (Objects.equals(token, Protocol.P2P.getType())) {
                result.add(tokens[i + 1]);
            }
        }
        return result;
    }

    public static byte[] decode(InetSocketAddress inetSocketAddress) {
        return decode(createAddress(inetSocketAddress));
    }

    public static byte[] decode(String address) throws Exception {
        String[] parts = address.split("/");
        if (parts.length == 0) {
            throw new Exception("Address has not a separator");
        }
        String empty = parts[0];
        if (!empty.isEmpty()) {
            throw new Exception("Address should start with separator '/'");
        }
        List<String> parsing = new ArrayList<>(Arrays.asList(parts).subList(1, parts.length));
        return decode(parsing);
    }

    public static String toAddress(List<String> parts) {
        return "/" + String.join("/", parts);
    }

    public static byte[] decode(List<String> parts) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            for (int i = 0; i < parts.size(); i++) {
                String part = parts.get(i);
                Protocol p = Protocol.get(part);
                p.appendCode(outputStream);
                if (p.size() == 0) continue;


                i++;
                String component = parts.get(i);
                if (component.length() == 0)
                    throw new IllegalStateException("Protocol requires address, but non provided!");

                outputStream.write(p.addressToBytes(component));
            }
            return outputStream.toByteArray();
        } catch (Exception exception) {
            throw new IllegalStateException("Error decoding multiaddress: " + toAddress(parts));
        }
    }

    public void appendCode(OutputStream out) throws IOException {
        out.write(encoded);
    }

    public int size() {
        return size;
    }

    public int code() {
        return code;
    }

    public String getType() {
        return type;
    }

    @NonNull
    @Override
    public String toString() {
        return name();
    }

    public byte[] addressToBytes(String addr) throws Exception {

        switch (this) {
            case IP4:
                if (!addr.matches(IPV4_REGEX))
                    throw new IllegalStateException("Invalid IPv4 address: " + addr);
                return Inet4Address.getByName(addr).getAddress();
            case IP6:
                return Inet6Address.getByName(addr).getAddress();
            case UDP:
                int x = Integer.parseInt(addr);
                if (x > 65535)
                    throw new IllegalStateException("Failed to parse " + name() + " address " + addr + " (> 65535");
                return new byte[]{(byte) (x >> 8), (byte) x};
            case P2P: {
                byte[] hashBytes = PeerId.fromBase58(addr).getBytes();

                try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    bout.write(varint);
                    bout.write(hashBytes);
                    return bout.toByteArray();
                }
            }
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR: {
                try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                    byte[] hashBytes = addr.getBytes();
                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    outputStream.write(varint);
                    outputStream.write(hashBytes);
                    return outputStream.toByteArray();
                }
            }
            default:
                throw new IllegalStateException("Unknown multiaddr type: " + name());
        }

    }

    public String readAddress(ByteBuffer in) throws Exception {
        int sizeForAddress = sizeForAddress(in);
        byte[] buf;
        switch (this) {
            case IP4:
            case IP6:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return InetAddress.getByAddress(buf).toString().substring(1);
            case UDP:
                int a = in.get() & 0xFF;
                int b = in.get() & 0xFF;
                return Integer.toString((a << 8) | b);
            case P2P:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return PeerId.create(buf).toBase58();
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return new String(buf);
        }
        throw new IllegalStateException("Unimplemented protocol type: " + type);
    }

    public int sizeForAddress(ByteBuffer in) throws IOException {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return DataHandler.readUnsignedVariant(in);
    }
}
