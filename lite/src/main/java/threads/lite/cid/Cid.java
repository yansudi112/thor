package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public final class Cid implements Comparable<Cid> {

    // codec
    public static final int IDENTITY = 0x00;
    public static final int Raw = 0x55;
    public static final int DagProtobuf = 0x70;
    public static final int Libp2pKey = 0x72;

    private final byte[] cid;

    private Cid(byte[] cid) {
        this.cid = cid;
    }

    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return new Cid(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return cid.bytes();
    }


    @NonNull
    public static Cid nsToCid(@NonNull String ns) throws Exception {
        return createCidV1(Raw, ns.getBytes(StandardCharsets.UTF_8));
    }

    @NonNull
    public static Cid decode(@NonNull String name) throws Exception {
        if (name.length() < 2) {
            throw new Exception("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            Multihash multihash = Multihash.deserialize(Base58.decode(name));
            Objects.requireNonNull(multihash);
            return new Cid(multihash.toBytes());
        }

        byte[] data = Multibase.decode(name);

        ByteBuffer wrap = ByteBuffer.wrap(data);

        int version = DataHandler.readUnsignedVariant(wrap);
        if (version != 1) {
            throw new Exception("invalid version");
        }
        int codecType = DataHandler.readUnsignedVariant(wrap);
        if (!(codecType == Cid.DagProtobuf || codecType == Cid.Raw ||
                codecType == Cid.Libp2pKey)) {
            throw new Exception("not supported codec");
        }

        Multihash mh = Multihash.deserialize(wrap);
        Objects.requireNonNull(mh);
        return new Cid(data);


    }

    @NonNull
    public static Cid createCidV0(byte[] data) throws Exception {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);
        Multihash multihash = new Multihash(Multihash.Type.sha2_256, hash);
        return new Cid(multihash.toBytes());
    }

    @NonNull
    public static Cid createCidV1(int codec, byte[] data) throws Exception {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);

        Prefix prefix = new Prefix(codec, hash.length, Multihash.Type.sha2_256.index, 1);
        byte[] prefixBytes = prefix.bytes();

        ByteBuffer buffer = ByteBuffer.allocate(prefixBytes.length + hash.length);
        buffer.put(prefixBytes);
        buffer.put(hash);
        return new Cid(buffer.array());
    }

    @NonNull
    public static Cid newCidV1(int codec, byte[] mhash) {
        int versionLength = DataHandler.unsignedVariantSize(1);
        int codecLength = DataHandler.unsignedVariantSize(codec);
        ByteBuffer buffer = ByteBuffer.allocate(versionLength + codecLength + mhash.length);
        DataHandler.writeUnsignedVariant(buffer, 1);
        DataHandler.writeUnsignedVariant(buffer, codec);
        buffer.put(mhash);
        return new Cid(buffer.array());
    }

    @NonNull
    public static Multihash encode(byte[] hash, Multihash.Type type) {
        return new Multihash(type, hash);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return Arrays.equals(this.cid, cid.cid);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(cid);
    }

    @NonNull
    public String String() {
        switch (getVersion()) {
            case 0:
                return Base58.encode(cid);
            case 1:
                return Multibase.encode(Multibase.Base.Base32, cid);
            default:
                throw new IllegalStateException("not supported version");
        }
    }

    public int getVersion() {
        byte[] bytes = cid;
        if (bytes.length == 34 && bytes[0] == 18 && bytes[1] == 32) {
            return 0;
        }
        return 1;
    }

    public int getCodec() throws IOException {
        return getPrefix().getCodec();
    }

    public byte[] bytes() {
        return cid;
    }


    @NonNull
    public Prefix getPrefix() throws IOException {
        if (getVersion() == 0) {
            return new Prefix(DagProtobuf, Multihash.Type.sha2_256.length,
                    Multihash.Type.sha2_256.index, 0);
        }
        return Prefix.getPrefixFromBytes(bytes());
    }

    @Override
    public int compareTo(Cid o) {
        return Integer.compare(this.hashCode(), o.hashCode());
    }

    // this function returns the multihash as bytes
    // a Multihash consists of three parts (Type, Length, Hash of Data)
    // Support is right now only for Type "sha2_256"
    public byte[] getMultihash() throws IOException {

        if (getVersion() == 0) {
            return cid;
        } else {
            // in case of version 1 (the version content and the codec has to be removed)
            ByteBuffer wrap = ByteBuffer.wrap(cid);
            DataHandler.readUnsignedVariant(wrap); // skip version
            DataHandler.readUnsignedVariant(wrap); // skip codec
            byte[] data = new byte[wrap.capacity() - wrap.position()];
            wrap.get(data);
            return data;
        }
    }

    public boolean isSupported() {
        try {
            return getPrefix().isSha2556();
        } catch (Throwable ignore) {
            return false;
        }
    }
}