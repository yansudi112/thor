package threads.lite.cid;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.utils.DataHandler;


public final class Prefix {

    private final int version;
    private final int codec;
    private final int type;
    private final int hashLength;

    public Prefix(int codec, int hashLength, int type, int version) {
        this.version = version;
        this.codec = codec;
        this.type = type;
        this.hashLength = hashLength;
    }

    public static Prefix getPrefixFromBytes(byte[] buf) throws IOException {
        ByteBuffer wrap = ByteBuffer.wrap(buf);

        int version = DataHandler.readUnsignedVariant(wrap);
        if (version != 1 && version != 0) {
            throw new IOException("invalid version");
        }
        int codec = DataHandler.readUnsignedVariant(wrap);
        if (!(codec == Cid.DagProtobuf || codec == Cid.Raw || codec == Cid.Libp2pKey)) {
            throw new IOException("not supported codec");
        }

        int mhtype = DataHandler.readUnsignedVariant(wrap);

        int mhlen = DataHandler.readUnsignedVariant(wrap);

        return new Prefix(codec, mhlen, mhtype, version);


    }

    @NonNull
    public static Cid sum(Prefix prefix, byte[] data) throws Exception {


        if (prefix.version == 0 && (!prefix.isSha2556()) ||
                (prefix.hashLength != 32 && prefix.hashLength != -1)) {
            throw new Exception("Invalid v0 Prefix");
        }

        if (!prefix.isSha2556()) {
            throw new IOException("Type Multihash " +
                    Multihash.Type.lookup(prefix.type).name() + " is not supported");
        }

        switch (prefix.version) {
            case 0:
                return Cid.createCidV0(data);
            case 1:
                return Cid.createCidV1(prefix.codec, data);
            default:
                throw new Exception("Invalid cid version");
        }


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prefix prefix = (Prefix) o;
        return version == prefix.version && codec == prefix.codec && type == prefix.type && hashLength == prefix.hashLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, codec, type, hashLength);
    }

    public Multihash.Type getType() {
        return Multihash.Type.lookup(type);
    }

    public boolean isSha2556() {
        return Multihash.Type.lookup(type) == Multihash.Type.sha2_256;
    }

    @NonNull
    @Override
    public String toString() {
        return "Prefix{" +
                "version=" + version +
                ", codec=" + codec +
                ", type=" + Multihash.Type.lookup(type).name() +
                ", hashLength=" + hashLength +
                '}';
    }

    public byte[] bytes() {
        int versionLength = DataHandler.unsignedVariantSize(version);
        int codecLength = DataHandler.unsignedVariantSize(codec);
        int typeLength = DataHandler.unsignedVariantSize(type);
        int length = DataHandler.unsignedVariantSize(hashLength);

        ByteBuffer buffer = ByteBuffer.allocate(versionLength + codecLength + typeLength + length);
        DataHandler.writeUnsignedVariant(buffer, version);
        DataHandler.writeUnsignedVariant(buffer, codec);
        DataHandler.writeUnsignedVariant(buffer, type);
        DataHandler.writeUnsignedVariant(buffer, hashLength);
        return buffer.array();
    }

    public int getVersion() {
        return version;
    }

    public int getCodec() {
        return codec;
    }
}
