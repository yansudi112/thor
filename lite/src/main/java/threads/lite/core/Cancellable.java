package threads.lite.core;


public interface Cancellable {
    boolean isCancelled();

    default long timeout() {
        return Long.MAX_VALUE;
    }
}
