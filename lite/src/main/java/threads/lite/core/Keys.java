package threads.lite.core;

import androidx.annotation.NonNull;


public class Keys {
    @NonNull
    private final byte[] publicKey;
    @NonNull
    private final byte[] privateKey;

    public Keys(@NonNull byte[] publicKey, @NonNull byte[] privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    @NonNull
    public byte[] getPublic() {
        return publicKey;
    }

    @NonNull
    public byte[] getPrivate() {
        return privateKey;
    }
}
