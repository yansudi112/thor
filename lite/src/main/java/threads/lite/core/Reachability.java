package threads.lite.core;

public enum Reachability {
    UNKNOWN, LOCAL, NONE, GLOBAL
}
