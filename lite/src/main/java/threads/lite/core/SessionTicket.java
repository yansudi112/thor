package threads.lite.core;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;


public class SessionTicket {
    private final byte[] data;

    public SessionTicket(byte[] data) {
        this.data = data;
    }

    @Nullable
    @TypeConverter
    public static SessionTicket fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return new SessionTicket(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(SessionTicket sessionTicket) {
        if (sessionTicket == null) {
            return null;
        }
        return sessionTicket.getData();
    }

    public byte[] getData() {
        return data;
    }
}
