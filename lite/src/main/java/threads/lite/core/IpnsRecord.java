package threads.lite.core;

public class IpnsRecord {

    private final byte[] ipnsKey;
    private final byte[] sealedRecord;

    public IpnsRecord(byte[] ipnsKey, byte[] sealedRecord) {
        this.ipnsKey = ipnsKey;
        this.sealedRecord = sealedRecord;
    }

    public byte[] getSealedRecord() {
        return sealedRecord;
    }

    public byte[] getIpnsKey() {
        return ipnsKey;
    }
}
