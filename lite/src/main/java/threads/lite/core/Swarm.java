package threads.lite.core;


import java.util.concurrent.CopyOnWriteArrayList;

public class Swarm extends CopyOnWriteArrayList<Connection> implements AutoCloseable {

    @Override
    public void close() {
        this.forEach(Connection::close);
    }
}
