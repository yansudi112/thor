package threads.lite.core;

import net.luminis.quic.TransportParameters;

import threads.lite.IPFS;

public class Parameters extends TransportParameters {

    public Parameters(int maxIdleTimeoutInSeconds,
                      int initialMaxStreamData,
                      int initialMaxStreamsBidirectional) {
        super(maxIdleTimeoutInSeconds, Integer.MAX_VALUE, initialMaxStreamData,
                initialMaxStreamsBidirectional, 0);

    }


    public static Parameters getDefaultUniDirection(int maxIdleTimeoutInSeconds,
                                                    int initialMaxStreamData) {
        return new Parameters(maxIdleTimeoutInSeconds, initialMaxStreamData,
                0);
    }

    public static Parameters getDefault() {
        return new Parameters(IPFS.GRACE_PERIOD, IPFS.MESSAGE_SIZE_MAX, IPFS.MAX_STREAMS);
    }
}
