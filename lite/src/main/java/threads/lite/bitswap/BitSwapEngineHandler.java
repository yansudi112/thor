package threads.lite.bitswap;

import java.nio.ByteBuffer;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.utils.DataHandler;

public class BitSwapEngineHandler implements ProtocolHandler {

    private final BitSwapEngine bitSwapEngine;

    public BitSwapEngineHandler(BitSwapEngine bitSwapEngine) {
        this.bitSwapEngine = bitSwapEngine;
    }

    @Override
    public String getProtocol() {
        return IPFS.BITSWAP_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.BITSWAP_PROTOCOL))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        bitSwapEngine.receiveMessage(stream.getConnection(),
                MessageOuterClass.Message.parseFrom(data.array()));
    }
}
