package threads.thor.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.magnet.Client;
import threads.magnet.ClientBuilder;
import threads.magnet.IdentityService;
import threads.magnet.Runtime;
import threads.magnet.event.EventBus;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.magnet.net.PeerId;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.MainActivity;
import threads.thor.R;
import threads.thor.core.Content;
import threads.thor.utils.ContentStorage;

public class DownloadMagnetWorker extends Worker {

    private static final String TAG = DownloadMagnetWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public DownloadMagnetWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    private static OneTimeWorkRequest getWork(@NonNull Uri magnet, @NonNull Uri dest) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.MAGNET, magnet.toString());
        data.putString(Content.URI, dest.toString());

        return new OneTimeWorkRequest.Builder(DownloadMagnetWorker.class)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();

    }

    public static void download(@NonNull Context context, @NonNull Uri magnet, @NonNull Uri dest) {
        WorkManager.getInstance(context).enqueue(getWork(magnet, dest));
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");

        try {

            String magnet = getInputData().getString(Content.MAGNET);
            Objects.requireNonNull(magnet);
            String dest = getInputData().getString(Content.URI);
            Objects.requireNonNull(dest);


            MagnetUri magnetUri = MagnetUriParser.lenientParser().parse(magnet);

            String name = magnet;
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }


            Uri uri = Uri.parse(dest);
            DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(), uri);
            Objects.requireNonNull(rootDocFile);


            DocumentFile find = rootDocFile.findFile(name);
            DocumentFile rootDoc;
            if (find != null && find.exists() && find.isDirectory()) {
                rootDoc = find;
            } else {
                rootDoc = rootDocFile.createDirectory(name);
            }

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.STORAGE_CHANNEL_ID);


            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentTitle(name)
                    .setSubText("" + 0 + "%")
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true);


            Notification notification = builder.build();

            int notificationId = getId().hashCode();

            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));

            try {
                Objects.requireNonNull(rootDoc);

                byte[] id = new IdentityService().getID();

                EventBus eventBus = Runtime.provideEventBus();
                ContentStorage storage = new ContentStorage(
                        getApplicationContext(), eventBus, rootDoc);
                Runtime runtime = new Runtime(PeerId.fromBytes(id), eventBus,
                        ContentStorage.nextFreePort());

                Client client = new ClientBuilder()
                        .runtime(runtime)
                        .storage(storage)
                        .magnet(magnet)
                        .build();

                AtomicInteger atomicProgress = new AtomicInteger(0);
                client.startAsync((torrentSessionState) -> {

                    long completePieces = torrentSessionState.getPiecesComplete();
                    long totalPieces = torrentSessionState.getPiecesTotal();
                    int progress = (int) ((completePieces * 100.0f) / totalPieces);

                    LogUtils.info(TAG, "progress : " + progress +
                            " pieces : " + completePieces + "/" + totalPieces);

                    if (atomicProgress.getAndSet(progress) < progress) {
                        builder.setSubText("" + progress + "%")
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }
                    if (isStopped()) {
                        try {
                            client.stop();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            LogUtils.info(TAG, "Client is stopped !!!");
                        }
                    }
                }, 1000).join();

                if (!isStopped()) {
                    storage.finish();

                    builder.setContentText(getApplicationContext().
                                    getString(R.string.download_complete, name))
                            .setSubText("")
                            .setProgress(0, 0, false);
                    notificationManager.notify(notificationId, builder.build());

                } else {
                    if (rootDoc.exists()) {
                        rootDoc.delete();
                    }
                }


            } catch (Throwable e) {
                if (!isStopped()) {
                    builder.setContentText(getApplicationContext()
                                    .getString(R.string.download_failed, name))
                            .setSubText("")
                            .setProgress(0, 0, false);
                    notificationManager.notify(notificationId, builder.build());
                }
                throw e;
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }

}
