package net.luminis.tls;

import android.util.SparseArray;

import net.luminis.tls.alert.DecodeErrorException;

public enum NamedGroup {

    /* Elliptic Curve Groups (ECDHE) */
    secp256r1(0x0017), secp384r1(0x0018), secp521r1(0x0019),

    /* Finite Field Groups (DHE) */
    ffdhe2048(0x0100), ffdhe3072(0x0101), ffdhe4096(0x0102),
    ffdhe6144(0x0103), ffdhe8192(0x0104),
    ;

    private static final SparseArray<NamedGroup> byValue = new SparseArray<>();

    static {
        for (NamedGroup t : NamedGroup.values()) {
            byValue.put(t.value, t);
        }
    }

    public final short value;

    NamedGroup(int value) {
        this.value = (short) value;
    }

    public static NamedGroup get(int value) throws DecodeErrorException {
        NamedGroup namedGroup = byValue.get(value);
        if (namedGroup == null) {
            throw new DecodeErrorException("invalid group value");
        }
        return namedGroup;
    }


}
