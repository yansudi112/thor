package threads.magnet.data.range;

/**
 * @since 1.3
 */
interface DelegatingRange {

    /**
     * @since 1.3
     */
    Range getDelegate();
}
