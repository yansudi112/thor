package threads.magnet.data.range;

import threads.magnet.data.BlockSet;

public class Ranges {


    public static BlockRange blockRange(Range range, long blockSize) {
        return new BlockRange(range, blockSize);
    }

    public static SynchronizedRange synchronizedRange(Range range) {
        return new SynchronizedRange(range);
    }

    public static BlockSet synchronizedBlockSet(BlockSet blockSet) {
        return new SynchronizedBlockSet(blockSet);
    }
}
