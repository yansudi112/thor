package threads.magnet.protocol;

import androidx.annotation.NonNull;

public final class Interested implements Message {

    private static final Interested instance = new Interested();

    private Interested() {
    }

    /**
     * @since 1.0
     */
    public static Interested instance() {
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "]";
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.INTERESTED_ID;
    }
}
